/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruction_4.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amamy <amamy@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 23:22:33 by qgirard           #+#    #+#             */
/*   Updated: 2020/02/11 00:06:15 by amamy            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"
#include "libft.h"

int     inst_aff(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

int     inst_lfork(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

int     inst_lldi(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

int     inst_lld(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruction_3.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amamy <amamy@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 23:22:29 by qgirard           #+#    #+#             */
/*   Updated: 2020/02/11 00:05:46 by amamy            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"
#include "libft.h"

int     inst_fork(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}
int     inst_sti(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}
int     inst_ldi(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}
int     inst_zjmp(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

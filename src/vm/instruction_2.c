/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruction_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amamy <amamy@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 23:22:13 by qgirard           #+#    #+#             */
/*   Updated: 2020/02/11 00:41:41 by amamy            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"
#include "libft.h"

int     inst_xor(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

int     inst_or(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

int     inst_and(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}

int     inst_sub(t_instruction **instruction, t_corewar *arena)
{
	(void)instruction;
	(void)arena;
    return (1);
}


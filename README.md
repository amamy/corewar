# Corewar

###### /!\ This project is not finished and does not fully works. /!\
**I show this as I am looking for internship and this is the last piece of code I have designed and written. This is a group project, hence, I did not code everything but I am the main designer of the code structure. I mainly worked on the asm part, lexer, parser and translator are designed by me, please refer to those parts. Only lexer and parser works nicely at the moment**.

---

### What is corewar
Corewar is a game in which **programs compete for control of a computer** called MARS (for Memory Array Redcode Simulator).  **Redcode** is the **name of the assembly language** in which Corewar programs, called warriors, are written.

In order to play the game, we need an **arena** which will host epics code battles : the **Virtual Machine** and and a way to **translate redcode** in **bytecode** (binary) : the assembler (shortened by **ASM**).

---

### The Assembler
An assembler is a special kind of compiler that takes assembly code and translate it into bytecode.
here is an example of a code for corewar : 
```
.name "zork"
.comment "just a basic living prog"
		
l2:	
sti	r1,%:live,%0
	and	r1,%0,r1
live:	live	%1
	zjmp	%:live

```

We can split it in two parts : 
- **header** -> the **.name** and the **.comment** lines
- **instruction section** -> everything else

##### 1) The Header 
The header is quite **simple in redcode**, it is just those two lines  :

```
.name "zork"
.comment "just a basic living prog"
```

The header has a specific encoding in bytecode that we have to apply to allow the vm to work properly.

##### 2) The Instruction section
This section is where the magic happens.
**Only two  items can compose a valid line** for our redcode asm : 
- label
- instruction

###### labels : 
Labels are used to refer to a line via a tag string.

Examble : 
```
00 l2:	
01 sti	r1,%:live,%0
```
Line 0 is a label declaration, from now on, if l2 is called, it will refers to the instruction on line 01.

###### Instructions : 
In our Corewar, champions have **16 instructions** at there disposal, which can have a maximum of **3 parameters each** among **3 types** :
- **Register** : a simulated processor register memory, form **r1 to r16 only**.
- **Indirect** : a **number** relative to the current instruction in memory.
- **Direct** : a **number** litteral.

Example :
```
sti	r1,15,%0
```
We here have an instruction *Store index* with as parameters : 
 1. : register 1
 2. : indirect, value = 15
 3. : direct, value = 0

 To put it simple : 

- a **number preceded by 'r'** : ----> *register*
- a **number preceded by '%'** :---> *direct number*
- a **number** : --------------------> *indirect number*


##### 3) Notes
The subject ask to display a relevant message in case of error, thanks to a **tokenizer/lexer and a parser**, we are able to process files until the last token and display errors for every lines.
###### Handled errors : 
The *lexer* allows us to detect **lexical errors** and the *parser* detects **syntax errors**.

All lexical errors will be tagged under "UNKNOWN_TOKEN" error.

|  Error Type				|  Displayed mesage | 
|---|---|
| UNKNOWN_TOKEN				| "Unknow token"  |
| MEMORY_ALLOCATION_ERROR	| "Could not allocate memory"  |
| BAD_OP_CODE  				| "Unrecognized instruction"  |
| INVALID_REGISTER  		| "Invalid register number, only exists r1 to r16"  |
| WRONG_ARGUMENT_TYPE  		| "This instruction does not take this type of parameter"  |
| NOT_ARGUMENT_TYPE  		|  "This is not a valid argument for any instruction" |
| MISSING_SEPARATOR			| ("Parameter given to instruction must be separated with a coma ','" |
| MISS_PLACED_SEPARATOR		| "Coma ',' are only used to separate instruction arguments"  |
| LABEL_REDEFINITION		| "LABEL_REDEFINITON : you cannot define same label twice"  |
| TOKEN_AFTER_OP			| "TOKEN_AFTER_OP : Unecessary token found after operation"  |
| UNDECLARED_LABEL_CALL		| "Use of undeclared label"  |
|   |   |

---